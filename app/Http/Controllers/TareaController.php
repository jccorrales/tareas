<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tarea;
use App\Http\Resources\Tarea as TareaResource;

class TareaController extends Controller
{
    //Función que despliega los objetos de tipo Tarea que se encuentren almacenados
    public function listarTareas() {
        //Se obtienen todos los registros pertenecientes al modelo Tarea
    	$tareas = Tarea::all();
        //En caso de que el modelo de tareas se encuentre vacío, se retorna un mensaje para indicar que no hay registros
    	if($tareas->isEmpty()) {
            //El código 404 indica que un recurso no fue encontrado
    		return response()->json(['message' => 'Sin registros'], 404);
    	}
        //Se utiliza la clase resource para customizar la manera en que se retornarán los datos pertenecientes al modelo Tarea
    	$coleccionTareas = TareaResource::collection($tareas);
        //El código 200 indica una petición exitosa
    	return response()->json($coleccionTareas, 200);
    }
    //Función que retorna un registro del modelo Tarea, recibe por parámetro el dato ID del objeto
    public function consultarTarea($tarea) {
        //Se obtiene el objeto del modelo Tarea filtrado por su ID
    	$tarea = Tarea::find($tarea);
        //En caso de no encontrarse el registro, se retorna el mensaje de recurso no encontrado
    	if(!$tarea) {
            //El código 404 indica que un recurso no fue encontrado
    		return response()->json(['message' => 'Tarea no encontrada'], 404);
    	}
        //Se utiliza la clase resource para customizar la manera en que se retornarán los datos del modelo Tarea
        //El código 200 indica una petición exitosa
    	return response()->json(new TareaResource($tarea), 200);
    }
    //Función que permite añadir un nuevo registro de tarea, acepta un objeto por medio del parámetro request
    public function agregarTarea(Request $request) {
        //Se validan los campos ingresados en la petición
        $request->validate([
            'nombre' => 'required|string',
            'descripcion' => 'required|string',
            'userID' => 'required|integer|exists:users,id',
        ]);
        //Se crea un objeto de tipo Tarea con los datos recibidos
        $tarea = new Tarea([
            'nombre' => $request->nombre,
            'descripcion' => $request->descripcion,
            'user_id' => $request->userID,
        ]);
        //Bloque try-catch para almacenar el registro
        try {
            $tarea->save();
        }
        //En caso de una falla al guardar el registro, se lanza un mensaje de error
        catch(\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
        //Se lanza un mensaje de código 201 indicando el registro exitoso acompañado de un enlace al recurso
        return response()->json([
            'message' => 'Tarea almacenada exitosamente',
            'link' => url('/api/tareas/'.$tarea->id),
        ], 201);
    }
     //Función para actualizar los datos de un registro de tipo Tarea, recibe por parámetro un objeto con la información
    //y un ID para localizar el registro
    public function actualizarDatosTarea(Request $request, $tarea) {
        //Se validan los campos ingresados en la petición
        $request->validate([
            'nombre' => 'required|string',
            'descripcion' => 'required|string',
            'userID' => 'required|integer|exists:users,id',
        ]);
        //Se obtiene el objeto del modelo Tarea filtrado por su ID
        $registroTarea = Tarea::find($tarea);
        //En caso de no encontrarse el registro, se retorna el mensaje de recurso no encontrado
        if(!$registroTarea) {
            //El código 404 indica que un recurso no fue encontrado
            return response()->json(['message' => 'Tarea no encontrada'], 404);
        }
        //Se asignan los nuevos datos al registro
        $registroTarea->nombre = $request->nombre;
        $registroTarea->descripcion = $request->descripcion;
        $registroTarea->user_id = $request->userID;
        //Bloque try-catch para almacenar el registro
        try {
            $registroTarea->save();
        }
        //En caso de una falla al guardar el registro, se lanza un mensaje de error
        catch(\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
        //Se lanza un mensaje de código 200 indicando la actualización exitosa acompañado de un enlace al recurso
        return response()->json([
            'message' => 'Datos de tarea actualizados exitosamente',
            'link' => url('/api/tareas/'.$registroTarea->id),
        ], 200);
    }
    //Función para eliminar un registro del modelo Tarea
    public function eliminarTarea($tarea) {
        //Se obtiene el objeto del modelo Tarea filtrado por su ID
        $registroTarea = Tarea::find($tarea);
        //En caso de no encontrarse el registro, se retorna el mensaje de recurso no encontrado
        if(!$registroTarea) {
            //El código 404 indica que un recurso no fue encontrado
            return response()->json(['message' => 'Tarea no encontrada'], 404);
        }
        //Bloque try-catch para almacenar el registro
        try {
            $registroTarea()->delete();
        }
        //En caso de una falla al guardar el registro, se lanza un mensaje de error
        catch(\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
        //Se lanza un mensaje de código 200 indicando la eliminación exitosa del recurso
        return response()->json(['message' => 'Tarea eliminada exitosamente'], 200);
    }
}