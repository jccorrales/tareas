<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Tarea as TareaResource;

class UserController extends Controller
{
    //Función que despliega los objetos de tipo User que se encuentren almacenados
    public function listarUsuarios() {
        //Se obtienen todos los registros pertenecientes al modelo User
    	$usuarios = User::all();
        //En caso de que el modelo de usuarios se encuentre vacío, se retorna un mensaje para indicar que no hay registros
    	if($usuarios->isEmpty()) {
            //El código 404 indica que un recurso no fue encontrado
    		return response()->json(['message' => 'Sin registros'], 404);
    	}
        //Se utiliza la clase resource para customizar la manera en que se retornarán los datos pertenecientes al modelo User
        $coleccionUsuarios = UserResource::collection($usuarios);
        //El código 200 indica una petición exitosa
    	return response()->json($coleccionUsuarios, 200);
    }
    //Función que retorna un registro del modelo User, recibe por parámetro el dato ID del objeto
    public function consultarUsuario($user) {
        //Se obtiene el objeto del modelo User filtrado por su ID
    	$usuario = User::find($user);
        //En caso de no encontrarse el registro, se retorna el mensaje de recurso no encontrado
    	if(!$usuario) {
            //El código 404 indica que un recurso no fue encontrado
    		return response()->json(['message' => 'Usuario no encontrado'], 404);
    	}
        //Se utiliza la clase resource para customizar la manera en que se retornarán los datos del modelo User
        //El código 200 indica una petición exitosa
    	return response()->json(new UserResource($usuario), 200);
    }
    //Función que retorna las tareas asignadas al usuario especificado por medio del parámetro ID
    public function consultarTareasUsuario($user) {
        //Se obtiene el objeto del modelo User filtrado por su ID
    	$usuario = User::find($user);
        //En caso de no encontrarse el registro, se retorna el mensaje de recurso no encontrado
    	if(!$usuario) {
            //El código 404 indica que un recurso no fue encontrado
    		return response()->json(['message' => 'Usuario no encontrado'], 404);
    	}
        //En caso de que el campo relacional de tareas se encuentre vacío, se retorna un mensaje para indicar que no hay registros
    	if($usuario->tareas->isEmpty()) {
            //El código 404 indica que un recurso no fue encontrado
    		return response()->json(['message' => 'No se encontraron tareas asignadas al usuario'], 404);
    	}
        //Se utiliza la clase resource para customizar la manera en que se retornarán los datos del modelo Tarea
    	$coleccionTareasUsuario = TareaResource::collection($usuario->tareas);
        //El código 200 indica una petición exitosa
    	return response()->json($coleccionTareasUsuario, 200);
    }
    //Función que permite añadir un nuevo registro de usuario, acepta un objeto por medio del parámetro request
    public function agregarUsuario(Request $request) {
        //Se validan los campos ingresados en la petición
    	$request->validate([
    		'name' => 'required|string',
    		'email' => 'required|string|email|unique:users',
    		'password' => 'required|string|min:6|confirmed',
    	]);
        //Se crea un objeto User con los datos recibidos
    	$usuario = new User([
    		'name' => $request->name,
    		'email' => $request->email,
    		'password' => $request->password,
    	]);
        //Bloque try-catch para almacenar el registro
    	try {
    		$usuario->save();
    	}
        //En caso de una falla al guardar el registro, se lanza un mensaje de error
    	catch(\Exception $e) {
    		return response($e->getMessage(), 500);
    	}
        //Se lanza un mensaje de código 201 indicando el registro exitoso acompañado de un enlace al recurso
    	return response()->json([
    		'message' => 'Usuario registrado exitosamente',
    		'link' => url('/api/users/'.$usuario->id),
    	], 201);
    }
    //Función para actualizar los datos de un registro de tipo User, recibe por parámetro un objeto con la información
    //y un ID para localizar el registro
    public function actualizarDatosUsuario(Request $request, $user) {
        //Se obtiene el objeto del modelo User filtrado por su ID
    	$usuario = User::find($user);
        //En caso de no encontrarse el registro, se retorna el mensaje de recurso no encontrado
    	if(!$usuario) {
            //El código 404 indica que un recurso no fue encontrado
    		return response()->json(['message' => 'Usuario no encontrado'], 404);
    	}
        //Se validan los campos ingresados en la petición
    	$request->validate([
    		'name' => 'required|string',
    		'password' => 'required|string|min:6|confirmed',
    		'email' => 'required|string|email|unique:users,email,'.$usuario->id,	
    	]);
        //Se asignan los nuevos datos al registro
    	$usuario->name = $request->name;
		$usuario->password = $request->password;
		$usuario->email = $request->email;
        //Bloque try-catch para almacenar el registro
		try {
			$usuario->save();
		}
        //En caso de una falla al guardar el registro, se lanza un mensaje de error
		catch(\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
        //Se lanza un mensaje de código 200 indicando la actualización exitosa acompañado de un enlace al recurso
		return response()->json([
			'message' => 'Datos de usuario actualizados exitosamente',
			'link' => url('/api/users/'.$usuario->id),
		], 200);
    }
    //Función para eliminar un registro del modelo User
    public function eliminarUsuario($user) {
        //Se obtiene el objeto del modelo User filtrado por su ID
    	$usuario = User::find($user);
        //En caso de no encontrarse el registro, se retorna el mensaje de recurso no encontrado
    	if(!$usuario) {
    		return response()->json(['message' => 'Usuario no encontrado'], 404);
    	}
        //Bloque try-catch para almacenar el registro
    	try {
    		$usuario->delete();
    	}
        //En caso de una falla al guardar el registro, se lanza un mensaje de error
    	catch(\Exception $e) {
    		return response()->json($e->getMessage(), 500);
    	}
        //Se lanza un mensaje de código 200 indicando la eliminación exitosa del recurso
    	return response()->json(['message' => 'Usuario eliminado exitosamente'], 200);
    }
}