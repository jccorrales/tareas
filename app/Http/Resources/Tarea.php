<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Tarea extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    //Función para retornar un arreglo con datos customizados del modelo Tarea
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'userID' => $this->user_id,
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'self' => url("/api/tareas/{$this->id}"),
            'user' => url("/api/users/{$this->user_id}"),
        ];
    }
}
