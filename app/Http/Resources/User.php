<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    //Función para retornar un arreglo con datos customizados del modelo User
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'self' => url("/api/users/{$this->id}"),
            'tareas' => url("/api/users/{$this->id}/tareas"),
        ];
    }
}
