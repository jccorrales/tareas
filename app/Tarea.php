<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
	//Se declaran los campos a los cuales será posible acceder para su llenado
    protected $fillable = [
    	'nombre',
    	'descripcion',
    	'user_id',
    ];
    //Mediante esta instrucción se protege el nombre de la tabla que se va a crear
    protected $table = 'tareas';
    //Función para retornar el usuario con el cual el modelo se encuentra relacionado
    public function user() {
    	return $this->belongsTo('App\User');
    }
}
