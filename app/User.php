<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //Campos a los que es posible acceder para su llenado
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
    //Campos que no se muestran al momento de una consulta del modelo
    protected $hidden = [
        'password', 'remember_token',
    ];
    //Función para retornar los objetos de tipo tarea relacionados con el usuario
    public function tareas() {
        return $this->hasMany('App\Tarea');
    }
}
