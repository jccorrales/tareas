<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Tarea::class, function (Faker $faker) {
	$user = User::inRandomOrder()->first();
    return [
        'nombre' => $faker->sentence(3),
        'descripcion' => $faker->paragraph,
        'user_id' => $user->id,
    ];
});
