<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::get('users', 'UserController@listarUsuarios');
Route::get('users/{user}', 'UserController@consultarUsuario');
Route::get('users/{user}/tareas', 'UserController@consultarTareasUsuario');
Route::get('tareas', 'TareaController@listarTareas');
Route::get('tareas/{tarea}', 'TareaController@consultarTarea');
Route::post('users', 'UserController@agregarUsuario');
Route::post('tareas', 'TareaController@agregarTarea');
Route::put('users/{user}', 'UserController@actualizarDatosUsuario');
Route::put('tareas/{tarea}', 'TareaController@actualizarDatosTarea');
Route::delete('users/{user}', 'UserController@eliminarUsuario');